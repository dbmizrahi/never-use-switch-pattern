# Never-Use-Switch pattern

## Spring @Service mapping

Based on solution from Evgeny Borisov's JavaDay Lviv presentation [Spring Patterns by Evgeny Borisov](https://youtu.be/ODzZeiAfI38) (russian)

We use the HashMap<String, Operation> where the String is Spring @Service unique Id and act with inbound request with no any switch or if statement.

All we need is call operations.get("key") and then we can use the exact service implementation.