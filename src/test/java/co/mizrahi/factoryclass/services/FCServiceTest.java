package co.mizrahi.factoryclass.services;

import co.mizrahi.factoryclass.dto.FCMessage;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@EnableWebMvc
@AutoConfigureMockMvc
class FCServiceTest {

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected ObjectWriter ow;

    protected MockHttpServletRequestBuilder setRequestBuilder(MockHttpServletRequestBuilder builder){
        return builder
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .characterEncoding("UTF-8");
    }

    @Test
    void messageHandler() throws Exception {
        FCMessage message = new FCMessage(1, "Message #1");
        MockHttpServletRequestBuilder request = this.setRequestBuilder(post("/messages")
                .content(ow.writeValueAsString(message)));
        this.mvc.perform(request).andExpect(status().is(200));
    }
}