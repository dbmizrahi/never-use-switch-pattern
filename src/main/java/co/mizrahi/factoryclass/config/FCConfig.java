package co.mizrahi.factoryclass.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static java.util.Objects.isNull;

@Configuration
public class FCConfig {

    private static ObjectMapper objectMapper;
    private static ObjectWriter objectWriter;

    @Bean
    ObjectMapper getObjectMapper(){
        if (!isNull(objectMapper)) return objectMapper;
        objectMapper = new ObjectMapper();
        return objectMapper;
    }

    @Bean
    ObjectWriter getObjectWriter(){
        if (!isNull(objectWriter)) return objectWriter;
        if ( isNull(objectMapper)) this.getObjectMapper();
        objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        return objectWriter;
    }
}
