package co.mizrahi.factoryclass.controllers;

import co.mizrahi.factoryclass.dto.FCMessage;
import co.mizrahi.factoryclass.services.IFCService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class FCController {

    private final IFCService service;

    @Autowired
    public FCController(IFCService service) {
        this.service = service;
    }

    @PostMapping(value = "/messages")
    public String messageHandler(@RequestBody FCMessage message) {
        return this.service.messageHandler(message);
    }
}
