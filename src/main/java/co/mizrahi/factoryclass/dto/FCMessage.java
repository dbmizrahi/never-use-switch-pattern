package co.mizrahi.factoryclass.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class FCMessage {
    private Integer key;
    private String message;
}
