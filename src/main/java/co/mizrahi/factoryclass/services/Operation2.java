package co.mizrahi.factoryclass.services;

import org.springframework.stereotype.Service;

@Service("2")
public class Operation2 implements Operation {
    @Override
    public String apply(String message) {
        return "This method invoked by key #2, incoming message: " + message;
    }
}
