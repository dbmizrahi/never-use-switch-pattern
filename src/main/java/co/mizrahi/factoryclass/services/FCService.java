package co.mizrahi.factoryclass.services;

import co.mizrahi.factoryclass.dto.FCMessage;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Service
public class FCService implements IFCService {

    private final Map<String, Operation> operationMap;

    @Autowired
    public FCService(Map<String, Operation> operationMap) {
        this.operationMap = operationMap;
    }

    @Override
    public String messageHandler(FCMessage message) {
        Operation targetOperation = operationMap.get(String.valueOf(message.getKey()));
        return targetOperation.apply(message.getMessage());
    }
}
