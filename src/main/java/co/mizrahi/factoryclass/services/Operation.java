package co.mizrahi.factoryclass.services;

public interface Operation {
    String apply(String message);
}
