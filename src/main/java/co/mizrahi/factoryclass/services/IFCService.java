package co.mizrahi.factoryclass.services;

import co.mizrahi.factoryclass.dto.FCMessage;

public interface IFCService {
    String messageHandler(FCMessage message);
}
