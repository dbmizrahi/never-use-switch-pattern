package co.mizrahi.factoryclass.services;

import org.springframework.stereotype.Service;

@Service("1")
public class Operation1 implements Operation {
    @Override
    public String apply(String message) {
        return "This method invoked by key #1, incoming message: " + message;
    }
}
