package co.mizrahi.factoryclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FCApp {

    public static void main(String[] args) {
        SpringApplication.run(FCApp.class, args);
    }

}
